package pojo;

import java.io.Serializable;

public class TaskUpdate implements Serializable
{

	private static final long serialVersionUID = 1L;
	private long taskId;
	private String update;
	private long when;
	
	public TaskUpdate(long orderId, String update) {
		super();
		this.taskId = orderId;
		this.update = update;
		this.when = System.currentTimeMillis();
	}
	
	public long getTaskId() {
		return taskId;
	}

	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}

	public String getUpdate() {
		return update;
	}
	public void setUpdate(String update) {
		this.update = update;
	}
	public long getWhen() {
		return when;
	}
	public void setWhen(long when) {
		this.when = when;
	}
	
	
}
