package pojo;

import java.util.Random;

public class Task {

	public Task() {
		// generate the orderId
		taskId = new Random().nextInt(1000);
	}
	public long getTaskId() {
		return taskId;
	}
	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}	

	private long taskId;	
}
