package tasks;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.core.MultivaluedHashMap;

import pojo.Task;
import pojo.TaskUpdate;

public class TaskManager implements Serializable {

	private static final long serialVersionUID = 1L;
	private static TaskManager taskMgr;
	private static final int MAX_PROCESSES = 2;
	private ExecutorService executor;

	// bounded queue to allow only 10 max order parallel processing
	private BlockingQueue<Task> pendingTasks = new LinkedBlockingDeque<Task>(10);
	private MultivaluedHashMap<Long, TaskUpdate> taskUpdates = new MultivaluedHashMap<Long, TaskUpdate>();

	private TaskManager() {
	}

	synchronized public static TaskManager getInstance() {
		if (taskMgr == null) {
			taskMgr = new TaskManager();
		}
		return taskMgr;
	}
	
	public void startProcessors() {
		executor = Executors.newFixedThreadPool(MAX_PROCESSES);
		for (int i = 0; i < MAX_PROCESSES; i++) {
			TaskProcessor worker = new TaskProcessor();
			executor.execute(worker);
		}
		System.out.println("processors configured");
	}

	public void stopProcessors() {

		executor.shutdown();
		try {
			if (!executor.awaitTermination(800, TimeUnit.MILLISECONDS)) {
				executor.shutdownNow();
			}
		} catch (InterruptedException e) {
			executor.shutdownNow();
		}
	}

	private void fakeProcessing() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public long addTask(Task task, String submitType) {
		if ("Online".equals(submitType)) {
			System.out.println("Processing online...");
			fakeProcessing();
			System.out.println("Processed");
		} else {
			this.addTaskUpdate(task.getTaskId(), "task submitted");
			try {
				this.pendingTasks.put(task);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return task.getTaskId();
	}

	public Task getNextTask() throws InterruptedException {
		return this.pendingTasks.take();
	}
	
	public void completeTask(long taskId) throws InterruptedException {
		this.deleteTaskUpdates(taskId);
	}
	public void deleteTaskUpdates(long taskId) {
		this.taskUpdates.remove(taskId);
	}

	public void addTaskUpdate(long taskId, String update) {
		this.taskUpdates.add(taskId, new TaskUpdate(taskId, update));
	}

	public List<TaskUpdate> getTaskUpdates(long taskId) {
		List<TaskUpdate> _list = this.taskUpdates.get(taskId);
		if(_list == null) {
			_list = new ArrayList<TaskUpdate>();
		}
		return _list;
	}

	public MultivaluedHashMap<Long, TaskUpdate> getTaskUpdates() {
		return this.taskUpdates;
	}

}