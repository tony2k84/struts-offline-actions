package tasks;

import pojo.Task;

public class TaskProcessor implements Runnable {

	private void fakeProcessing() {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			System.out.println("thread interrupted while processing");
		}
	}

	@Override
	public void run() {
		try {
			while (true) {
				System.out.println(Thread.currentThread().getName() + "-->" + "waiting for the task...");
				Task task = TaskManager.getInstance().getNextTask();
				System.out.println(Thread.currentThread().getName() + "-->" + "processing task...");
				TaskManager.getInstance().addTaskUpdate(task.getTaskId(), "processing started");
				fakeProcessing();
				TaskManager.getInstance().addTaskUpdate(task.getTaskId(), "order validated");
				fakeProcessing();
				TaskManager.getInstance().addTaskUpdate(task.getTaskId(), "step 1 completed");
				fakeProcessing();
				TaskManager.getInstance().addTaskUpdate(task.getTaskId(), "step 2 completed");
				fakeProcessing();
				TaskManager.getInstance().addTaskUpdate(task.getTaskId(), "task completed");
				TaskManager.getInstance().completeTask(task.getTaskId());

			}
		} catch (InterruptedException e) {
			System.out.println("thread interrupted while global processing");
		}
		System.out.println("thread completed");
	}

}
