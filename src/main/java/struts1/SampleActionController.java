package struts1;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import pojo.Task;
import tasks.TaskManager;

public class SampleActionController extends Action{
	
	public ActionForward execute(ActionMapping mapping,ActionForm form,
		HttpServletRequest request,HttpServletResponse response)
        throws Exception {
		
		SampleActionForm sampleForm = (SampleActionForm) form;
		long taskid = TaskManager.getInstance().addTask(new Task(), "Online");
		sampleForm.setTaskId(taskid);
		return mapping.findForward("success");
	}
	
}
