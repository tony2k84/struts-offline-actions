package struts1;

import org.apache.struts.action.ActionForm;

public class SampleActionForm extends ActionForm{

	private static final long serialVersionUID = 1L;
	
	private long taskId;

	public long getTaskId() {
		return taskId;
	}

	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}

	
}
