package api;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MultivaluedHashMap;

import pojo.TaskUpdate;
import tasks.TaskManager;

@Path("/task")
public class TaskService {

	@GET
	@Path("/status")
	@Produces("application/json")
	public List<TaskUpdate> getStatus(@QueryParam("taskId") long taskId) {
		return TaskManager.getInstance().getTaskUpdates(taskId);
	}

	@GET
	@Path("/status/all")
	@Produces("application/json")
	public MultivaluedHashMap<Long, TaskUpdate> getStatus() {
		return TaskManager.getInstance().getTaskUpdates();
	}
}
