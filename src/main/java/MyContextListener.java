import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import tasks.TaskManager;

public class MyContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
    	System.out.println("init started");
        TaskManager.getInstance().startProcessors();
        System.out.println("init completed");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    	System.out.println("clean up started");
    	TaskManager.getInstance().stopProcessors();
        System.out.println("clean up completed");
    }
}