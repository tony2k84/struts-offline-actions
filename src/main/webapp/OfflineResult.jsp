<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1,shrink-to-fit=no">
<meta name="theme-color" content="#000000">
<link href="https://fonts.googleapis.com/css?family=Open+Sans"
	rel="stylesheet">
<title>Offline Task Status Update</title>
<link href="/struts-offline-actions/static/css/main.bf842c01.css"
	rel="stylesheet">
</head>
<body>
	<noscript>You need to enable JavaScript to run this app.</noscript>
	<div id="root" _taskid=<bean:write name="sampleActionForm" property="taskId" format="#" />></div>
	<script type="text/javascript"
		src="/struts-offline-actions/static/js/main.6538ebc0.js"></script>
</body>
</html>